# Instalando e configurando o Tomcat
**Após ter realizado os passos na implantação do [servidor-web](https://github.com/cmachadox/desafios/blob/master/desafio-1/servidor-web.md), vamos instalar o Tomcat**

## Install Java

```
# yum install -y java-11-openjdk-devel

# java -version

openjdk version "11.0.7" 2020-04-14 LTS
OpenJDK Runtime Environment 18.9 (build 11.0.7+10-LTS)
OpenJDK 64-Bit Server VM 18.9 (build 11.0.7+10-LTS, mixed mode, sharing)

```

**Criando um usuário para o Tomcat.**

```
# useradd -r tomcat
```
**Em seguida, criaremos uma pasta e, em seguida, usaremos o comando “ cd ” para mudar os diretórios para a pasta em que faremos o download do Tomcat.**
```
# mkdir /usr/local/tomcat9
# cd /usr/local/tomcat9
```
Agora, vamos fazer o download do arquivo Tomcat usando curl. (o wget também é uma opção se estiver instalado na sua versão do CentOS 7. Caso contrário, você poderá instalá-lo para uso posterior
```
# curl -o tomcat9.tar.gz http://www-us.apache.org/dist/tomcat/tomcat-9/v9.0.30/bin/apache-tomcat-9.0.30.tar.gz
# tar -xvf tomcat9.tar.gz
# chown -R tomcat.tomcat /usr/local/tomcat9

```
**Definir variável de ambiente**

Agora, podemos configurar a variável de ambiente CATALINA_HOME usando os seguintes comandos:
```
# echo "export CATALINA_HOME="/usr/local/tomcat9"" >> ~/.bashrc
# source ~/.bashrc
```
Essa variável é configurada para garantir que o acesso ao software seja permitido para todos os usuários do seu sistema.


**Criar e configurar o serviço Systemd**
```
# vim /etc/systemd/system/tomcat.service
```
Agora, podemos adicionar as seguintes informações ao nosso novo arquivo de unidade.
```
[Unit]
Description=Apache Tomcat Server
After=syslog.target network.target

[Service]
Type=forking
User=tomcat
Group=tomcat

Environment=CATALINA_PID=/usr/local/tomcat9/temp/tomcat.pid
Environment=CATALINA_HOME=/usr/local/tomcat9
Environment=CATALINA_BASE=/usr/local/tomcat9

ExecStart=/usr/local/tomcat9/bin/catalina.sh start
ExecStop=/usr/local/tomcat9/bin/catalina.sh stop

RestartSec=10
Restart=always
[Install]
WantedBy=multi-user.target
```
Depois disso, precisamos salvar o arquivo (usando: wq) e recarregar o serviço para aplicar as alterações.
```
# systemctl daemon-reload
```
Em seguida, vamos iniciar o serviço Tomcat e habilitá-lo. 
```
# systemctl start tomcat.service && systemctl enable tomcat.service

# firewall-cmd --zone=public --permanent --add-port=8080/tcp
```
Teste

Em seguida, vamos abrir um navegador para testar a conexão. 

Carregue http://server-IP:8080 no seu navegador e você deverá ver uma imagem como a abaixo.
![](https://mluh4on6k7db.i.optimole.com/h1xpWcI-WHePzVeM/w:652/h:564/q:90/https://www.liquidweb.com/kb/wp-content/uploads/2020/02/apache.tomcat.9.install2.4.20old.png)

Execute um comando netstat para verificar se o servidor está atendendo aos endereços IPv4 (e / ou endereço IPv6, se necessário).

Após verificar o netstat, vimos que o Tomcat estava vinculado apenas às portas do protocolo IPv6.

```
# netstat -nlp | grep java

tcp6       0      0 127.0.0.1:8005          :::*                    OUÇA       966/java            
tcp6       0      0 :::8080                 :::*                    OUÇA       966/java
````
Para resolver isso, crie um arquivo chamado setenv.sh no diretório bin / CATALINA_BASE ou CATALINA_HOME
(usamos a pasta /usr/local/tomcat9/bin).

Em seguida, crie um novo arquivo chamado setenv.sh
```
# vim setenv.sh
```
Adicione a seguinte linha ao arquivo.
```
JAVA_OPTS="$JAVA_OPTS -Djava.net.preferIPv4Stack=true -Djava.net.preferIPv4Addresses=true "
```
Esta entrada substitui as configurações padrão usadas pelo Tomcat, salve e saia do arquivo e recarregue os serviços.
```
# systemctl daemon-reload && systemctl restart tomcat
```
Agora, vamos verificar as portas novamente.

```
# netstat -ntpl | grep java

tcp        0      0 127.0.0.1:8005          0.0.0.0:*               OUÇA       15021/java          
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               OUÇA       15021/java
```
**Configurar o Nginx como um proxy reverso**

```
# vim /etc/nginx/conf.d/tomcat.conf
```
Adicione as seguintes linhas:
```
upstream tomcat {
 server 127.0.0.1:8080 weight=100 max_fails=5 fail_timeout=5;
 }
 
 server {
 listen 80;
 server_name tomcat.tuxmaluco.com.br;
 
 location / {
 proxy_set_header X-Forwarded-Host $host;
 proxy_set_header X-Forwarded-Server $host;
 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 proxy_pass http://tomcat/;
 }
 }
```
Parabéns! Agora você pode acessar o servidor Tomcat usando o URL http://seudominio.com sem especificar a porta 8080 do Tomcat.

***Configurando acesso https***
```
# certbot --nginx -d tomcat.seudominio.com
````

Selecione a opção 2 para redirecionar os trafegos http para https








