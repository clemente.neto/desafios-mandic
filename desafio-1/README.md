## Atividade 01

- Subir em uma máquina centos7 na Amazon - EC2
- Criar o dominio e configurar route53 Dominio: **http://www.dot.tk/pt/index.html**
- Configurar SSL Free
Obs: O site, o blog wordpress, a loja magento, deve cada um deles ser 1 virtualhost do
nginx e cada 1 deles deverá responder pelo endereço a quem ele pertence, cada um
deles deverá ter a sua pasta dentro de **/var/www/html/NOME-DA-PASTA**

Anotem todos os comandos em um bloco de notas.

## Criação SITE

Este site exemplo deve responder pelo endereço site-NOME.DOMINIO.tk.​ , crie lá um
index.php com conteúdo " ​ Conseguir chegar (eu já apontei o IP da sua máquina para
este endereço DNS, então quando chamar ele já vai para o seu IP, você deve
configurar o virtualhost do nginx para entender que quando chegar neste nome, ele
deve abrir essa página)

## Criação BLOG

Configure agora um blog pra você em wordpress, chame de Blog do ​ NOME​ , a URL
desse blog será
blog-NOME​ .DOMINIO.tk.​ . ​ .
Este endereço também já está apontando para o IP do seu servidor

Obs: Quando eu chamar no browser ​ blog-NOME​ .DOMINIO.tk deverá abrir o site, não
pode abrir com /wordpress na frente.Criação LOJA
Configure agora uma loja magento chamada ​ loja-blog-NOME​ .DOMINIO.tk​ .
Este endereço também já está apontado para o IP do seu servidor.

## Criação Tomcat

Baixe um arquivo zip ou tar.gz do tomcat, coloque ele na pasta /opt, entre na pasta do
binário, execute o tomcat, ele irá subir na porta 8080, chame no seu browser o IP do
servidor na porta 8080, veja se abre a tela do tomcat, se abrir, faça então um proxy
reverso pelo nginx que quando a requisição chegar para
tomcat-NOME​ .DOMINIO.tk​ . ​ , ele passe para este servidor tomcat na porta 8080
Obs: Este servidor não deve conter apache em execução, para saber se existe ou não,
execute o comando: ps -ef | grep -i http
Boa Sorte.


## INICIANDO AQUI;

ETAPA 1: [Configurando servidor Web](https://gitlab.com/clemente.neto/desafios-mandic/-/blob/master/desafio-1/config-servidor-web.md)

ETAPA 2: [Instalando e configurando Magento2](https://gitlab.com/clemente.neto/desafios-mandic/-/blob/master/desafio-1/magento2/README.md)

ETAPA 3: [Instalado e configurando Wordress](https://gitlab.com/clemente.neto/desafios-mandic/-/blob/master/desafio-1/wordpress/README.md)

ETAPA 4: [Configurando Vhost para o Site](https://gitlab.com/clemente.neto/desafios-mandic/-/blob/master/desafio-1/site/README.md)

ETAPA 5: [Instalando e configurando o Tomcat](https://gitlab.com/clemente.neto/desafios-mandic/-/blob/master/desafio-1/tomcat/README.md)
