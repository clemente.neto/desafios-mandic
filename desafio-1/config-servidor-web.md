# Servidor Web Centos 7 

## Esse servidor irá hospedar 3 aplicações cada uma respondendo em seu Vhosts.

## Criamos e configuramos um dominio no route53: **www.tuxmaluco.com.br**


### Vamos iniciar o processo de instalação e configuração do NGINX+PHP-FPM+Vistual Hosts

#### Instalando pacotes necessatios para o Centos.

```
# yum install -y epel-release

# yum install -y net-tools wget vim unzip curl

```
Configurar o SELinux e o Firewalld
```
Deixaremos o SELinux no modo 'Execução'. Execute os comandos abaixo para verificar o status do SELinux

# sestatus
```
![imagem](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/10.png)

O SELinux está no modo 'Executando'.

```

# yum -y install firewalld
# systemctl start firewalld && systemctl enable firewalld

Abra as portas para HTTP e HTTPS para que possamos acessar a URL a partir de um navegador da web.

# firewall-cmd --permanent --add-service=http
# firewall-cmd --permanent --add-service=https
# firewall-cmd --reload
# firewall-cmd --list-all
```
![imagem](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/11.png)

HTTP e HTTPS estão prontos para conexões.


#### Instalando o NGINX, criei o arquivo abaixo adicionando o conteúdo

```
# vim /etc/yum.repos.d/nginx.repo

[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=0
enabled=1

# yum -y install nginx
# systemctl enable nginx && systemctl start nginx

O Nginx é iniciado na porta 80, você pode verificar isso com o comando netstat abaixo.

# netstat -plntu
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/1.png)

Acessando via browser você verá que seu Nginx está iniciado

http://seu-ip


### Instalar os pacotes php e configurar o PHP-FPM/PHP.INI

Faça o download e inclua um novo repositório webtatic no sistema.

```
# rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

Agora execute o comando abaixo para instalar o PHP-FPM7 com todos os pacotes e extensões.

# yum -y install php70w-fpm php70w-mcrypt php70w-curl php70w-cli php70w-mysql php70w-gd php70w-xsl php70w-json php70w-intl php70w-pear php70w-devel php70w-mbstring php70w-zip php70w-soap
```

Quando uma instalação do PHP-FPM7 estiver concluída, precisamos configurá-la. Vamos configurar o arquivo php.ini e o arquivo 'www.conf' da configuração do php-fpm.

Edite o php.ini com o vim.
```
# vim /etc/php.ini
```
Remova o comentário da linha cgi.fix_pathinfo 762 e altere o valor para 0.
```
cgi.fix_pathinfo=0

```
Configure o limite de memória, o tempo máximo de execução e ative a compactação de saída zlib, certifique-se de definir os valores conforme mostrado abaixo.

```
memory_limit = 512M
max_execution_time = 1800
zlib.output_compression = On
```
Remova o comentário do diretório do caminho da sessão e altere o diretório como abaixo.
```
session.save_path = "/var/lib/php/session"

```
Salvar e sair.

Em seguida, edite o arquivo de configuração php-fpm www.conf com o comando vim.

```
# vim /etc/php-fpm.d/www.conf

```
O PHP-FPM7 será executado no usuário e grupo 'nginx', altere o valor para 'nginx' para as linhas de usuário e grupo.

```
user = nginx
group = nginx
```
Executaremos o php-fpm em um arquivo de soquete, não em uma porta do servidor. Altere o valor da linha de escuta para o caminho do arquivo de soquete.
```
listen = /var/run/php/php-fpm.sock
```
O arquivo de soquete precisa pertencer ao usuário e grupo 'nginx', descomente as linhas de escuta e altere os valores conforme mostrado abaixo.
```
listen.owner = nginx
listen.group = nginx
listen.mode = 0660

```
Por fim, remova o comentário do ambiente do PHP-FPM, linhas 366-370.
```
env[HOSTNAME] = $HOSTNAME
env[PATH] = /usr/local/bin:/usr/bin:/bin
env[TMP] = /tmp
env[TMPDIR] = /tmp
env[TEMP] = /tmp
```

Salvar e sair.

Em seguida, crie um novo diretório para o caminho da sessão e o local do arquivo php sock. Em seguida, altere o proprietário para o usuário e grupo 'nginx'.
```
# mkdir -p /var/lib/php/session/
# chown -R nginx:nginx /var/lib/php/session/
```
Crie um novo diretório para o local do arquivo de soquete php-fpm.
```
# mkdir -p /run/php/
# chown -R nginx:nginx /run/php/
```
A configuração do PHP-FPM7 está concluída, inicie o daemon agora e ative-o no momento da inicialização com o comando systemctl abaixo.
```
# systemctl start php-fpm && systemctl enable php-fpm

```
Quando não há erros, você pode usar o comando abaixo para verificar se o php-fpm está em execução no arquivo de soquete.
```
netstat -pl | grep php-fpm.sock
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/2.png)

### Configurando o Nginx para utilizar o PHP-FPM

Vamos configurar o NGINX para reconhecer o PHP_FPM, para isso vamos editar o seu arquivo de configuração default para o nosso cenário.
```
vim /etc/nginx/conf.d/default.conf

```
Adicione/descomente os seguintes parâmetros:
```
location ~ \.php$ {
        root           /usr/share/nginx/html;
        fastcgi_pass   unix:/run/php/php-fpm.sock;
        fastcgi_index  index.php;
	fastcgi_param  SCRIPT_FILENAME         $document_root$fastcgi_script_name;
        include        fastcgi_params;
```

### Segue um exemplo do arquivo completo default.conf do NGINX+PHP-FPM:
```
server {
    listen       80;
    server_name  localhost;
 
    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;
 
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
 
    #error_page  404              /404.html;
 
    # redirect server error pages to the static page /50x.html
    #
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
 
    # pass the PHP scripts to FastCGI server listening on unix:/run/php/php-fpm.sock;
    #
    location ~ \.php$ {
        root           /usr/share/nginx/html;
        fastcgi_pass   unix:/run/php/php-fpm.sock;
        fastcgi_index  index.php;
    fastcgi_param  SCRIPT_FILENAME         $document_root$fastcgi_script_name;
        include        fastcgi_params;
    }
 
    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    #
    #location ~ /\.ht {
    #    deny  all;
    #}
}
```

Para validar a nossa instalação do PHP nada melhor do que o bom e velho info.php
```
vim /usr/share/nginx/html/info.php

Adicione o conteúdo abaixo no arquivo e salve.

<?php phpinfo(); ?>
```
Reinicie o nginx e php-fpm.
```
systemctl restart nginx && systemctl restart php-fpm
```
Ao reiniciar abra seu browser novamente, e basta conferir a instalação realizada com sucesso!

http://seu-ip/info.php


### Instalando o cliente Let's Encrypt

```
# yum install certbot python2-certbot-nginx
```
Gere seus parâmetros DH com o OpenSSL:
```
# cd /etc/ssl/certs
# openssl dhparam -out dhparam.pem 4096
```
Let's Encrypt instalado e pronto para gerar seus certifcados`



