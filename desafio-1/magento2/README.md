# Instalando e configurando o Magento2
**Após ter realizado os passos na implantação do [servidor-web](https://github.com/cmachadox/desafios/blob/master/desafio-1/servidor-web.md), vamos instalar o Magento2**

_Antes de mais nada crie uma conta na pagina do magento (https://account.magento.com/customer/account/login) para que possa gerar sua key para instala a a plicação_

* Create a New Access Key
* Copie sua ""Private Key""


### Vamos começar instalando o servidor MySQL 5.7

Magento 2.1 requer uma versão atual do MySQL, você pode usar o MySQL 5.6 ou 5.7 para a instalação. Neste tutorial, usaremos o MySQL 5.7, que pode ser instalado no repositório oficial do MySQL. Então, precisamos adicionar o novo repositório MySQL primeiro.

Faça o download e adicione o novo repositório MySQL para instalação do MySQL 5.7.

```
# yum localinstall https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm

```
Agora instale o MySQL 5.7 a partir do repositório MySQL com o comando yum abaixo.

```
# yum install mysql-community-server
```
Quando a instalação estiver concluída, inicie o mysql e adicione-o para iniciar no momento da inicialização.

```
# systemctl start mysqld && systemctl enable mysqld
```

O MySQL 5.7 foi instalado com uma senha root padrão, e é armazenado no arquivo mysqld.log. Use o comando grep para ver a senha padrão do MySQL 5.7. Execute o comando abaixo.
```
# grep 'temporary' /var/log/mysqld.log
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/3.png)

Agora você vê a senha raiz atual do MySQL, altere a senha padrão.

Conecte-se ao shell do mysql com a raiz do usuário e a senha padrão.
```
# mysql -u root -p
TYPE DEFAULT PASSWORD
```

Altere a senha root padrão com a consulta abaixo. Vou usar a nova senha ' Hakase-labs123 @ ' aqui. Por favor, escolha uma senha segura.

```
ALTER USER 'root'@'localhost' IDENTIFIED BY 'Hakase-labs123@';
flush privileges;
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/4.png)

A senha root do MySQL padrão foi alterada. Em seguida, precisamos criar um novo usuário e um novo banco de dados para a instalação do Magento.

Criaremos um novo banco de dados chamado 'magentodb' e um novo usuário 'magentouser' com a senha ' Magento123 @ '. Em seguida, conceda todos os privilégios do banco de dados ao novo usuário. Execute a consulta mysql abaixo.
```
create database magentodb;
create user magentouser@localhost identified by 'Magento123@';
grant all privileges on magentodb.* to magentouser@localhost identified by 'Magento123@';
flush privileges;
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/5.png)

A instalação e configuração do MySQL 5.7 estão concluídas e um banco de dados para a instalação do Magento foi criado.

### Agora vamos instalar e configurar o Magento

Nesta etapa, começaremos a instalar e configurar o Magento. Para o diretório raiz da web, usaremos o diretório '/usr/share/nginx/html/magento2'. Precisamos do compositor PHP para a instalação do pacote Magento.

#### Instale o PHP Composer

Usaremos o Composer para instalação de dependências de pacotes PHP. Instale o compositor com o comando curl abaixo.
```
# curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer
```
Quando a instalação estiver concluída, você pode verificar a versão do compositor, como mostrado abaixo.
```
# composer -v
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/6.png)

O PHP Composer foi instalado.

**Faça o download e extraia o Magento**

Vá para o diretório '/usr/share/nginx/html' e faça o download do código Magento com o comando wget.
```
# cd /usr/share/nginx/html

# wget https://github.com/magento/magento2/archive/2.1.zip
```
Extraia o código Magento e renomeie para 'magento2'.
```
# unzip 2.1.zip
# mv magento2-2.1 magento2
```
**Instalar dependências do PHP**

Vá para o diretório magento2 e instale todas as dependências do Magento com o comando composer.
```
# cd magento2
# composer install -v
```
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/7.png)

Aguarde a instalação da Dependência do PHP terminar.

**Configurar host virtual Magento**

Vá para o diretório Nginx e crie um novo arquivo de configuração de host virtual chamado 'magento.conf' no diretório 'conf.d'.
```
# cd /etc/nginx/conf.d
# vim magento.conf
```
Cole a configuração do host virtual abaixo.
```
upstream fastcgi_backend {
        server  unix:/run/php/php-fpm.sock;

}

server {
        server_name loja.tuxmaluco.com.br www.loja.tuxmaluco.com.br;
        set $MAGE_ROOT /usr/share/nginx/html/magento2;
        set $MAGE_MODE developer;
        include /usr/share/nginx/html/magento2/nginx.conf.sample;
}
```
Salvar e sair.

Agora teste a configuração. Quando não houver erro, reinicie o serviço Nginx.
```
# nginx -t
# systemctl restart nginx
````
![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/8.png)

**Instale o Magento 2.1**

Vá para o diretório magento2 para instalar o Magento na linha de comando.
```
# cd /usr/share/nginx/html/magento2
```
Execute o comando abaixo, verifique se você tem a configuração correta. Os valores que precisam ser alterados por você são explicados abaixo.

```
bin/magento setup:install --backend-frontname="adminlogin" \
--key="biY8vdWx4w8KV5Q59380Fejy36l6ssUb" \
--db-host="localhost" \
--db-name="magentodb" \
--db-user="magentouser" \
--db-password="Magento123@" \
--language="en_US" \
--currency="USD" \
--timezone="America/New_York" \
--use-rewrites=1 \
--use-secure=0 \
--base-url="http://magento.hakase-labs.com" \
--base-url-secure="https://magento.hakase-labs.com" \
--admin-user=adminuser \
--admin-password=admin123@ \
--admin-email=admin@newmagento.com \
--admin-firstname=admin \
--admin-lastname=user \
--cleanup-database
```
Alterar valor para:

* backend-frontname: página de login do Magento admin "adminlogin"
* key: sua private key - gerada la no começo
* db-host: host local do banco de dados
* db-name: nome do banco de dados 'magentodb'
* db-user: usuário do banco de dados 'magentouser'
* db-password: senha do banco de dados ' Magento123 @ '
* base-url: sua URL do Magento
* admin-user: seu usuário administrador
* admin-password: sua senha de administrador do Magento
* admin-email: seu endereço de email do site magento

Quando você não tiver nenhum erro no comando, verá os resultados abaixo.

![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/9.png)


Magento 2.1 está instalado. Execute o comando abaixo para alterar a permissão do diretório etc e altere o proprietário do diretório magento2 para o usuário 'nginx'
```
# chmod 700 /usr/share/nginx/html/magento2/app/etc
# chown -R nginx:nginx /usr/share/nginx/html/magento2
```
Configurar Magento Cron

Este cronjob é necessário para o indexador Magento. Crie um novo cronjob como usuário 'nginx' (porque o magento está sendo executado no usuário e grupo nginx).

```
# crontab -u nginx -e

* * * * * /usr/bin/php /usr/share/nginx/html/magento2/bin/magento cron:run | grep -v "Ran jobs by schedule" >> /usr/share/nginx/html/magento2/var/log/magento.cron.log
* * * * * /usr/bin/php /usr/share/nginx/html/magento2/update/cron.php >> /usr/share/nginx/html/magento2/var/log/update.cron.log
* * * * * /usr/bin/php /usr/share/nginx/html/magento2/bin/magento setup:cron:run >> /usr/share/nginx/html/magento2/var/log/setup.cron.log
```
Salvar e sair.

Instale a ferramenta de gerenciamento do SELinux 'policycoreutils-python' com o comando yum abaixo.
```
# yum -y install policycoreutils-python
```
Vá para o diretório '/usr/share/nginx/html'.
```
# semanage fcontext -a -t httpd_sys_rw_content_t '/usr/share/nginx/html/magento2(/.*)?'
# semanage fcontext -a -t httpd_sys_rw_content_t '/usr/share/nginx/html/magento2/app/etc(/.*)?'
# semanage fcontext -a -t httpd_sys_rw_content_t '/usr/share/nginx/html/magento2/var(/.*)?'
# semanage fcontext -a -t httpd_sys_rw_content_t '/usr/share/nginx/html/magento2/pub/media(/.*)?'
# semanage fcontext -a -t httpd_sys_rw_content_t '/usr/share/nginx/html/magento2/pub/static(/.*)?'
# restorecon -Rv '/usr/share/nginx/html/magento2/'
```
**Teste**

Abra seu navegador da Web e, em seguida, navegue no URL do Magento. O meu é:

http://loja.tuxmaluco.com.br/

E você verá a página inicial padrão do Magento.

![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/12.png)

Em seguida, efetue login no painel do administrador abrindo o URL do adminlogin.

https://loja.tuxmaluco.com.br/adminlogin

Entre com seu usuário e senha de administrador.

![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/13.png)

![](https://www.howtoforge.com/images/how_to_install_magento_commerce_2-1_on_centos_7/14.png)

**O Magento 2.1 foi instalado com sucesso com o Nginx, PHP-FPM7 e MySQL 5.7 em um servidor CentOS 7.**


### Configurando acesso https

```
certbot --nginx -d loja.seudominio.com
```

**Selecione a opção 2 para redirecionar os trafegos http para https**


**Obs:** _Após redirecioinar meu trafego para https obtive um erro "Error ERR_TOO_MANY_REDIRECTS"
para resoler esse problema tive que fazer o seginte;_

```
# mysql -uroot -p
SUASENHA

> use bancomagento;
> mysql -e 'select * from core_config_data onde caminho rlike "base_url";
> update core_config_data set value = "https://SUA-URL-HTTPS" where path rlike "web /.*/ base_url";
> flush privileges;
> exit;

Dentro do diretorio magento2 rode o;

# cd usr/share/nginx/html/magento2
# php bin/magento cache:flush
```
