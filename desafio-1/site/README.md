# Configurando Vhost para o SITE

**Após ter realizado os passos na implantação do [servidor-web](https://github.com/cmachadox/desafios/blob/master/desafio-1/servidor-web.md), vamos configurar um virtual host para hospedar o SITE**

## Vamos criar um diretorio para o site dentro da pasta root do NGINX
```
# cd /usr/share/nginx/html
# mkdir site
# chmod -R 755 site 
# chown -R nginx.nginx site
```
**Em seguida vamos criar o arquivo de vhost para o path do site**
```
# cd /etc/nginx/conf.d
# cp default.conf site.conf
# vim site.conf
```
Vamos adicionar o **server name*** e add o **index.php** no location > index e alterar todas as linhas **"root /usr/share/nginx/html/"** do arquivo para **"/usr/share/nginx/html/site"** path do nosso blog wordpress.
```
server {
    listen       80;
    server_name  site.tuxmaluco.com.br www.site.tuxmaluco.com.br;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html/site;
        index  index.html index.htm index.php;
    }

```
**Configurando acesso https**
```
# certbot --nginx -d site.seudominio.com
```

Selecione a opção 2 para redirecionar os trafegos http para https
