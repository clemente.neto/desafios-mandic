# Instalando e configurando Blog Wordpress

**Após ter realizado os passos na implantação do [servidor-web](https://github.com/cmachadox/desafios/blob/master/desafio-1/servidor-web.md), vamos instalar o Wordpress**

Vamos criar uma base de dados, vamos entrar no MariaDB com o comando:
```
# mysql -u root -p(a mesma usada na conf do magento2)

> CREATE DATABASE db_wordpress;
> GRANT ALL PRIVILEGES on db_wordpress.* to 'utilizador'@'localhost' identified by 'password';
> FLUSH PRIVILEGES;
exit

```
Vamos agora proceder á instalação da última versão do WordPress:

```
# cd /usr/share/nginx/html
# wget http://wordpress.org/latest.tar.gz
# tar -xvf latest.tar.gz
```
Renomeie para o nome de sua escolha, no meu caso alterei para "blog"
```
# mv wordpress blog
```
De seguida vamos criar um diretório para upload de ficheiros:
```
# mkdir blog/wp-content/uploads
```
Depois definimos as permissões:
```
# chown -R nginx:nginx blog/
# chmod -R 755 blog/
````
Vamos usar o ficheiro wp-config-sample.php para definirmos a configuração. 
Criamos o ficheiro wp-config.php com base no ficheiro wp-config-sample.php e para esses passos todos vamos utilizar os comandos:
````
# cd blog
# cp wp-config-sample.php wp-config.php
# vim wp-config.php
````
Dentro do ficheiro coloquem o:

* DB_NAME’, ‘db_wordpress’
* DB_USER’, ‘O nome que quer de utilizador’
* DB_PASSWORD’, ‘E a password que quer’

### Vamos criar o virtual host para essa pagina do blog wordpress

```
# cd /etc/nginx/conf.d
# cp default.conf blog.conf
# vim blog.conf
```
Vamos adicionar o **server name** e add o **index.php** no **location > index** e alterar todas as linhas **root /usr/share/nginx/html/** para **"/usr/share/nginx/html/blog"** path do nosso blog wordpress. 

```
server {
    listen       80;
    server_name  blog.tuxmaluco.com.br www.blog.tuxmaluco.com.br;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

    location / {
        root   /usr/share/nginx/html/blog;
        index  index.html index.htm index.php;
    }
    
 ```

Agora tem que colocar o ip da sua máquina no browser. 

**OBS:** Caso apareça“Forbidden You don’t have permission to access / on this server.” caso isso aconteça vá ao terminal e escreva o comando:

```
setenforce 0
```

**Agora fazemos a configuração pedida como:**


![](https://miro.medium.com/max/1400/1*AcAra2Am29irznwhmx_yoA.png)

Depois de todas as configurações irá aparecer:

![](https://miro.medium.com/max/1400/1*bNs62b_rsPdf20MJhsn5Tg.png)

Agora já tem acesso ao painel e a todas as funcionalidades do seu site através do wordpress.

![](https://miro.medium.com/max/1400/1*2N-lfAqHZ5Jnuf3vGQoaSA.png)

Para aceder ao seu site pesquise o endereço ip do seu servidor no browser.
Para aceder ao painel e ás suas funcionalidades basta acrescentar a extensão:
```
/wp-admin
```
Ou seja:

http://seu-endereço/wp-admin
-------

**Configurando acesso https**
````
# certbot --nginx -d blog.seudominio.com
````

Selecione a opção 2 para redirecionar os trafegos http para https
